const initialState = {
    fname: 'Parameswar',
    lname: 'Mondal',
    email: 'param23.03@gmail.com',
    hobbies: ['Cricket', 'Biking', 'Travelling', 'Movie', 'Music'],
}

const nameReducer= (state= '', action) => {
    console.log('action ',action);
    if(action.type==='CHANGE_NAME'){
        //let fullname= `${action.payload.firstname} ${action.payload.lastname}`;
        return action.payload;
    }
    return state;
}

export default nameReducer;