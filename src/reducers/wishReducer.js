const initialState = {
    fname: 'Parameswar',
    lname: 'Mondal',
    email: 'param23.03@gmail.com',
    hobbies: ['Cricket', 'Biking', 'Travelling', 'Movie', 'Music'],
}

const wishReducer= (state= [], action) => {
    console.log('action ',action);
    if(action.type==='ADD_WISH'){
        return [...state, action.payload];
    }
    return state;
}

export default wishReducer;