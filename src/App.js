import React from 'react';
import './App.css';
import {connect} from 'react-redux';
import {anotherName, addWish} from './action/myaction';

function App(props) {
  console.log(props);
  const name= `${props.name} and your email-ID:  param23.03`;
  const mywish= props.mywish.map((wish, index) => {
    return <h2 key={index}>{wish}</h2>;
  });
  return (
    <div className="App">
      <h2>Redux Tutorial</h2>
      <h3>Welcome {name}</h3>
      {mywish}
      <button onClick={() => {props.changeName('Kalyan', 'Pal')}}>Change Name</button>
      <button onClick={() => {props.addWish()}}>Add Wish</button>
    </div>
  );
}

const mapStateToProps= (state) => {
  return {
    name: state.name,
    mywish: state.wish,
  }
}; 

const mapDespatchToProps= (dispatch) =>{
  return {
    changeName: (fname, lname) => {dispatch( anotherName(fname, lname) )},
    addWish: () => {dispatch(addWish())}
  }
};
export default connect(mapStateToProps, mapDespatchToProps)(App);
