import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';  
import thunk from 'redux-thunk';

import nameReducer from './reducers/nameReducer';      // This is my root reducer
import wishReducer from './reducers/wishReducer';
const rootReducer= combineReducers({
    name: nameReducer,
    wish: wishReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;  // For redux store extension

const store= createStore(rootReducer,{name: 'Ramesh', wish: ['Eat', 'Sleep'] }, composeEnhancers(applyMiddleware(thunk)));             // Create Store

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
