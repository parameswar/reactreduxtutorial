// export const anotherName= (fname, lname) => {
//     return {
//         type: 'CHANGE_NAME', 
//         payload: {firstname: fname, lastname: lname}
//     }  
// }

export const anotherName= (fname, lname) => {
    return (despatch) => {
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(res2 => {
            despatch({
                type: 'CHANGE_NAME', 
                payload: res2[0].name,

            })
        })
    }  
}

export const addWish= (fname, lname) => {
    return {
        type: 'ADD_WISH',
        payload: 'code'
    }
}